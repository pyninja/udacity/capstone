import os
import json

basedir = os.path.abspath(os.path.dirname(__file__))
ROWS_PER_PAGE = 10


class Config:
    SECRET_KEY = os.getenv('SECRET_KEY', 'dirty_little_secret')
    DEBUG = False


class DevelopmentConfig(Config):
    # uncomment the line below to use postgres
    # SQLALCHEMY_DATABASE_URI = postgres_local_base
    db_identifier = 'postgres://'
    conn_str = "datapsycho:admin1@localhost:5432/udacity"
    SQLALCHEMY_DATABASE_URI = db_identifier + conn_str
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    DEBUG = True


class ProductionConfig(Config):
    DEBUG = False
    # uncomment the line below to use postgres
    # SQLALCHEMY_DATABASE_URI = postgres_local_base
    SQLALCHEMY_DATABASE_URI = os.environ.get("DATABASE_URL")
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    DEBUG = False


config_by_name = dict(
    dev=DevelopmentConfig,
    prod=ProductionConfig
)

key = Config.SECRET_KEY


def get_barer_token():
    with open("token.json", encoding="utf-8") as f:
        data = json.load(f)
        return data

