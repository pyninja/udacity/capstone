import os
import unittest
import json
from flask_sqlalchemy import SQLAlchemy
from app import create_app
from models import setup_db
from config import config_by_name, get_barer_token


tokens = get_barer_token()


class AgencyTestCase(unittest.TestCase):
    """This class represents the agency test case"""

    def setUp(self):
        """Define test variables and initialize app."""

        self.app = create_app(config_by_name, "dev")
        self.client = self.app.test_client
        setup_db(self.app)
        with self.app.app_context():
            self.db = SQLAlchemy()
            self.db.init_app(self.app)
            # create all tables
            self.db.create_all()

    def tearDown(self):
        """Executed after reach test"""
        pass

    def test_create_new_actor(self):
        """Test POST new actor."""

        actor = {
            "name": "Newman",
            "age": 25
        }

        res = self.client().post(
            "/actors",
            json=actor,
            headers=tokens.get("producer")
        )
        data = json.loads(res.data)

        self.assertEqual(res.status_code, 200)
        self.assertTrue(data["success"])

    def test_get_all_actors(self):
        """Test GET all actors."""
        res = self.client().get(
            "/actors?page=1",
            headers=tokens["assistant"]
        )
        data = json.loads(res.data)

        self.assertEqual(res.status_code, 200)
        self.assertTrue(data["success"])

    def test_error_401_new_actor(self):
        """Error while posting with out Bearer token"""

        actor = {
            "name": "Newman",
            "age": 25
        }

        res = self.client().post("/actors", json=actor)
        data = json.loads(res.data)

        self.assertEqual(res.status_code, 401)
        self.assertFalse(data["success"])

    def test_error_422_create_new_actor(self):
        """Check for any missing attribute in json body."""

        actor = {
            "age": 25
        }

        res = self.client().post(
            "/actors",
            json=actor,
            headers=tokens["director"]
        )
        data = json.loads(res.data)

        self.assertEqual(res.status_code, 422)
        self.assertFalse(data["success"])

    def test_edit_actor(self):
        """Test PATCH existing actors"""
        actor = {
            "age": 25
        }
        res = self.client().patch(
            "/actors/1",
            json=actor,
            headers=tokens["director"]
        )
        data = json.loads(res.data)
        self.assertEqual(res.status_code, 200)
        self.assertTrue(data["success"])

    def test_error_401_delete_actor(self):
        """Test delete actor without Authorization"""
        res = self.client().delete("/actors/1")
        data = json.loads(res.data)
        self.assertEqual(res.status_code, 401)
        self.assertFalse(data["success"])

    # Movie Test
    def test_create_new_movie(self):
        """Test POST new movie with persona director."""

        movie = {
            "title": "Newman Movie",
            "release_date": "2020-11-11"
        }

        res = self.client().post(
            "/movies",
            json=movie,
            headers=tokens["director"]
        )
        data = json.loads(res.data)
        self.assertEqual(res.status_code, 200)
        self.assertTrue(data["success"])

    def test_error_422_create_new_movie(self):
        """Test Error POST new movie for persona producer."""

        movie = {
            "release_date": "2020-11-11"
        }

        res = self.client().post(
            "/movies",
            json=movie,
            headers=tokens["producer"]
        )
        data = json.loads(res.data)

        self.assertEqual(res.status_code, 422)
        self.assertFalse(data["success"])

    def test_get_all_movies(self):
        """Test GET all movies for persona assistant."""
        res = self.client().get(
            "/movies?page=1",
            headers=tokens["assistant"]
        )
        data = json.loads(res.data)

        self.assertEqual(res.status_code, 200)
        self.assertTrue(data["success"])

    def test_edit_movie(self):
        """Test PATCH existing movies for persona producer"""
        movie = {
            "release_date": "2020-12-12"
        }
        res = self.client().patch(
            "/movies/1",
            json=movie,
            headers=tokens["producer"]
        )
        data = json.loads(res.data)

        self.assertEqual(res.status_code, 200)
        self.assertTrue(data["success"])

    def test_delete_movie(self):
        """Test DELETE existing movie for producer"""
        res = self.client().delete(
            "/movies/3",
            headers=tokens["producer"]
        )
        data = json.loads(res.data)
        self.assertEqual(res.status_code, 200)
        self.assertTrue(data["success"])

    def test_error_401_delete_movie(self):
        """Test DELETE existing movie without Authorization"""
        res = self.client().delete("/movies/1")
        data = json.loads(res.data)
        self.assertEqual(res.status_code, 401)
        self.assertFalse(data["success"])


if __name__ == "__main__":
    unittest.main()
