# The Casting Agency App: Kollywood
`Kollywood` is the backend api application project for fulfilment of `Udacity` fullstack nano degree. This is the final capstone projects. Main app server :
`https://kollywood.herokuapp.com/`. Visiting the following link with lead to
Auth 0 authentication and redirect back to welcome page.

The project combined all the following knowledge learned from the nano degree
and put in to one practical application.

- Coding in Python 3
- Relational Database Architecture
- Modeling Data Objects with SQLAlchemy
- Internet Protocols and Communication
- Developing a Flask API
- Authentication and Access
- Authentication with Auth0
- Authentication in Flask
- Role-Based Access Control (RBAC)
- Testing Flask Applications
- Deploying Applications

The application is hosted into heroku an can be accessible.

## Kollywood

`kollywood` is an API having data about `Movies`, `Actors` and link between
which actor is working on which movie. Though in the current version of app.
it is not possible to access the information about actors and movies link. It
is suitable for casting agency where people have different people have
different roles such as `Director`, `Producer` and `Assistant` and each role
has restricted access in the movie and artist database. Based on Auth0
authentication scheme they are able or not able to do certain operation from
the following:

- Endpoints:
    - GET /actors and /movies
    - DELETE /actors/ and /movies/
    - POST /actors and /movies and
    - PATCH /actors/ and /movies/

- Roles:
    - Casting Assistant
        - Can view actors and movies
    - Casting Director
        - All permissions a Casting Assistant has and…
        - Add or delete an actor from the database
        - Modify actors or movies
    - Executive Producer
        - All permissions a Casting Director has and…
        -Add or delete a movie from the database

## Endpoint RBAC Check
The end point RBAC check is done through Postman and the result can be found in
the Postman test folder. There is also a unit test has been to check the
following errors:

- One test for success behavior of each endpoint
- One test for error behavior of each endpoint
- At least two tests of RBAC for each role

## Software Requirements and Setup
The software is written on python3.6 and postgres database server. The
application then deployed on a Heroku server for easily accessible for
everyone. If you want to try our the app please clone the repo and do the
following:
- Create a Virtual Environment with python3.6
- Install all the dependency
- Change the database connection string in the config file or add it from
    environ variable
- Create a schema `agency` in the database
- initiate the app by running `FLASK_APP=app.py && flask run --reload`

## API Endpoints
After setting up the app it will serve the following endpoints. But each
request must have valid Authorization header and the person should be have
right to do such operation. The main application url is
`https://kollywood.herokuapp.com/`

```shell script
# Example Authorization header
{"Authorization": "Bearer eyJhbGciO....}
```

Error Handling
Errors are returned as JSON objects in the following format:

```json
{
    "success": false,
    "error": 400,
    "message": "bad request"
}
```

The API will return three error types when requests fail:

- 400: Bad Request
- 404: Resource Not Found
- 422: Not Processable
- Other 40X+ authentication errors

### Get/actors

- General
    - Return all available actors
- sample: `https://kollywood.herokuapp.com/actors?page=1`

```json
{
    "actors": [
        {
            "age": 40,
            "gender": "Other",
            "id": 2,
            "name": "Crazy Kim"
        },
        {
            "age": 25,
            "gender": "Other",
            "id": 1,
            "name": "Cool Chember"
        }
    ],
    "success": true,
    "total": 2
}
```

### Get-Movies

- General
    - Return all available movies
- sample: `curl -X GET https://kollywood.herokuapp.com/movies?page=1`

```json
{
    "movies": [
        {
            "id": 1,
            "release_date": "Wed, 11 Nov 2020 00:00:00 GMT",
            "title": "Qurantine Holocaust"
        },
        {
            "id": 2,
            "release_date": "Wed, 11 Nov 2020 00:00:00 GMT",
            "title": "Psychophobic Qurantine"
        }
    ],
    "success": true,
    "total": 2
}
```

### post/actors

- General
    - Insert actors in the database
- sample
    - `curl -X POST https://kollywood.herokuapp.com/actors`
- Request Headers: (application/json), string name (required),
    integer age (required), string gender
- Requires permission: create:actors

```json
{
    "created": 5,
    "success": true
}
```
### post/movies
- General
    - Insert movies in the database
- sample
    - `curl -X POST https://kollywood.herokuapp.com/movies`
- Request Headers: (application/json) string title (required)
    string release_date (required)
- Requires permission: create:movies

```json
{
    "created": 5,
    "success": true
}
```

### PATCH/actors

- General
    - Update existing actor
- sample
    - `curl -X PATCH https://kollywood.herokuapp.com/actors`
- Request Headers: (application/json) string name, integer age, string gender
- Requires permission: edit:actors
```json
{
    "actor": [
        {
            "age": 30,
            "gender": "Other",
            "id": 2,
            "name": "Crazy Kim"
        }
    ],
    "success": true,
    "updated": 2
}
```
### PATCH/movies
- General
    - Update existing movie
- sample
    - `curl -X PATCH https://kollywood.herokuapp.com/movies`
- Request Headers: (application/json) string title, string release_date
- Requires permission: edit:movies

```json
{
    "edited": 2,
    "movie": [
        {
            "id": 2,
            "release_date": "Fri, 27 Nov 2020 00:00:00 GMT",
            "title": "Psychophobic Qurantine"
        }
    ],
    "success": true
}
```

### delete/actors

- General
    - Delete existing actor
- sample
    - `curl -X DELETE https://kollywood.herokuapp.com/actors/1`
- Request Arguments: integer id from actor
- Requires permission: delete:actors

```json
{
    "deleted": 5,
    "success": true
}
```

###delete/movies
- General
    - Delete existing movie
- sample
    - `curl -X DELETE https://kollywood.herokuapp.com/movies/1`
- Request Arguments: integer id from actor
- Requires permission: delete:movies

```json
{
    "deleted": 5,
    "success": true
}
```

## Courtesy
- Udacity
- My Udacity Mentor: Akshit A
- Amazing Udacity Classmates
