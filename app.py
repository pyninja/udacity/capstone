from flask import Flask, jsonify, request, abort
from config import config_by_name
from models import setup_db, Actor, Movie
from flask_cors import CORS
from auth import AuthError, requires_auth
import datetime as dt

ITEM_PER_PAGE = 10


def paginate_result(request_obj, selection):
    page = request_obj.args.get('page', 1, type=int)
    start = (page - 1) * ITEM_PER_PAGE
    end = start + ITEM_PER_PAGE
    contents = [content.format() for content in selection]
    current_contents = contents[start:end]
    return current_contents


def create_app(config_obj=config_by_name, config_name=None):
    if config_name:
        app = Flask(__name__)
        app.config.from_object(config_obj[config_name])
        setup_db(app)
        CORS(app)
    else:
        app = Flask(__name__)
        setup_db(app)
        CORS(app)

    @app.after_request
    def after_request(response):
        response.headers.add(
            'Access-Control-Allow-Headers',
            'Content-Type,Authorization,true'
        )
        response.headers.add(
            'Access-Control-Allow-Methods',
            'GET,PATCH,POST,DELETE,OPTIONS'
        )
        return response

    # =================
    # Authentication Only valid for live server in heroku
    # ==================
    @app.route("/")
    def redirect_auth():
        auth_domain = os.environ.get("AUTH0_DOMAIN")
        audiance = os.environ.get("API_AUDIENCE")
        client_id = os.environ.get("CLIENT_ID")
        redirect_url = "https://kollywood.herokuapp.com/welcome"
        url_1 = "https://{}/authorize?audience={}&".format(
            auth_domain,
            audiance
        )
        url_2 = "response_type=token&client_id={}&redirect_uri={}".format(
            client_id,
            redirect_url
        )
        return redirect(url_1+url_2)

    @ app.route("/welcome")
    def welcome():
        return "You are Authenticated !"

    @app.route('/actors', methods=['GET'])
    @requires_auth('read:actors')
    def fetch_actors(payload):
        try:
            actor_obj = Actor.query.all()
            actors = paginate_result(request, actor_obj)
            return jsonify({
                'success': True,
                'actors': actors,
                "total": len(actor_obj)
            })
        except Exception as e:
            print(e)
            abort(404)

    @app.route('/actors', methods=['POST'])
    @requires_auth('create:actors')
    def insert_actors(payload):
        body = request.get_json()

        if not body:
            abort(400)

        if "name" in body and "age" in body:
            try:
                name = body.get('name', None)
                age = body.get('age', None)
                gender = body.get('gender', 'Other')
                # Create new instance of Actor & insert it.
                new_actor = (Actor(
                    name=name,
                    age=age,
                    gender=gender
                ))
                new_actor.insert()

                return jsonify({
                    'success': True,
                    'created': new_actor.id
                })
            except Exception as e:
                print(e)
                abort(422)
        else:
            abort(422)

    @app.route('/actors/<actor_id>', methods=['PATCH'])
    @requires_auth('edit:actors')
    def edit_actors(payload, actor_id):
        # Get request json
        body = request.get_json()

        # Abort if no actor_id or body has been provided
        if not actor_id:
            abort(400,
                  {'message': 'please append an actor id to the request url.'})

        if not body:
            abort(400,
                  {'message': 'request does not contain a valid JSON body.'})

        # Find actor which should be updated by id
        actor = Actor.query.filter(
            Actor.id == actor_id).one_or_none()

        # Abort 404 if no actor with this id exists
        if not actor:
            abort(404, {
                'message': 'Actor with id {} not found in database.'.format(
                    actor_id)})

        try:
            name = body.get('name', actor.name)
            age = body.get('age', actor.age)
            gender = body.get('gender', actor.gender)

            # Set new field values
            actor.name = name
            actor.age = age
            actor.gender = gender
            # Update actor with new values
            actor.update()

            return jsonify({
                'success': True,
                'updated': actor.id,
                'actor': [actor.format()]
            })
        except Exception as e:
            print(e)
            abort(422)

    @app.route('/actors/<actor_id>', methods=['DELETE'])
    @requires_auth('delete:actors')
    def delete_actors(payload, actor_id):
        # Abort if no actor_id has been provided
        if not actor_id:
            abort(400,
                  {'message': 'please append an actor id to the request url.'})

        # Find actor which should be deleted by id
        actor = Actor.query.filter(
            Actor.id == actor_id).one_or_none()

        # If no actor with given id could found, abort 404
        if not actor:
            abort(404, {
                'message': 'Actor with id {} not found in database.'.format(
                    actor_id)})

        # Delete actor from database
        try:
            actor.delete()

            # Return success and id from deleted actor
            return jsonify({
                'success': True,
                'deleted': actor_id
            })
        except Exception as e:
            print(e)
            abort(422)

    @app.route('/movies', methods=['GET'])
    @requires_auth('read:movies')
    def get_movies(payload):
        try:
            movie_obj = Movie.query.all()
            movies = paginate_result(request, movie_obj)
            return jsonify({
                'success': True,
                'movies': movies,
                "total": len(movie_obj)
            })
        except Exception as e:
            print(e)
            abort(404)

    @app.route('/movies', methods=['POST'])
    @requires_auth('create:movies')
    def insert_movies(payload):
        # Get request json
        body = request.get_json()

        if not body:
            abort(400,
                  {'message': 'request does not contain a valid JSON body.'})

        # Extract title and release_date value from request body
        title = body.get('title', None)
        release_date = body.get('release_date', None)

        # abort if one of these are missing with appropiate error message
        if not title:
            abort(422, {'message': 'no title provided.'})

        if not release_date:
            abort(422, {'message': 'no "release_date" provided.'})

        # Create new instance of movie & insert it.
        try:
            py_release_date = dt.datetime.strptime(release_date, "%Y-%m-%d")
            movie = Movie(
                title=title,
                release_date=py_release_date.date()
            )
            movie.insert()

            return jsonify({
                'success': True,
                'created': movie.id
            })
        except Exception as e:
            print(e)
            abort(422)

    @app.route('/movies/<movie_id>', methods=['PATCH'])
    @requires_auth('edit:movies')
    def edit_movies(payload, movie_id):
        # Get request json
        body = request.get_json()

        # Abort if no movie_id or body has been provided
        if not movie_id:
            abort(400,
                  {'message': 'please append an movie id to the request url.'})

        if not body:
            abort(400,
                  {'message': 'request does not contain a valid JSON body.'})

        # Find movie which should be updated by id
        movie = Movie.query.filter(
            Movie.id == movie_id).one_or_none()

        # Abort 404 if no movie with this id exists
        if not movie:
            abort(404, {
                'message': 'Movie with id {} not found in database.'.format(
                    movie_id)})

        # Extract title and age value from request body
        # If not given, set existing field values, so no update will happen
        try:
            title = body.get('title', movie.title)
            release_date = body.get('release_date', movie.release_date)

            # Set new field values
            movie.title = title
            movie.release_date = release_date

            # Delete movie with new values
            movie.update()

            # Return success, updated movie id and updated
            # movie as formatted list
            return jsonify({
                'success': True,
                'edited': movie.id,
                'movie': [movie.format()]
            })
        except Exception as e:
            print(e)
            abort(422)

    @app.route('/movies/<movie_id>', methods=['DELETE'])
    @requires_auth('delete:movies')
    def delete_movies(payload, movie_id):
        # Abort if no movie_id has been provided
        if not movie_id:
            abort(400,
                  {'message': 'please append an movie id to the request url.'})

        # Find movie which should be deleted by id
        movie = Movie.query.filter(
            Movie.id == movie_id).one_or_none()

        # If no movie with given id could found, abort 404
        if not movie:
            abort(404, {
                'message': 'Movie with id {} not found in database.'.format(
                    movie_id)})

        # Delete movie from database
        try:
            movie.delete()

            # Return success and id from deleted movie
            return jsonify({
                'success': True,
                'deleted': movie_id
            })
        except Exception as e:
            print(e)
            abort(422)

    # ==============
    # Error Handlers
    # ==============
    @app.errorhandler(422)
    def unprocessable(error):
        return jsonify({
            "success": False,
            "error": 422,
            "message": "unprocessable"
        }), 422

    @app.errorhandler(400)
    def bad_request(error):
        return jsonify({
            "success": False,
            "error": 400,
            "message": "bad request"
        }), 400

    @app.errorhandler(404)
    def ressource_not_found(error):
        return jsonify({
            "success": False,
            "error": 404,
            "message": "resource not found"
        }), 404

    @app.errorhandler(AuthError)
    def authentication_failed(excp):
        return jsonify({
            "success": False,
            "error": excp.status_code,
            "message": "Authentication error check the request header."
        }), excp.status_code

    return app
