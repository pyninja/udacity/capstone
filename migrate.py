from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from models import db
from app import create_app
from config import config_by_name

app = create_app(config_by_name, "prod")

migrate = Migrate(app, db)
manager = Manager(app)
manager.add_command('db', MigrateCommand)


if __name__ == '__main__':
    manager.run()
