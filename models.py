from sqlalchemy import Column, String, Integer, Date, Table, Float
from sqlalchemy import ForeignKey
from flask_sqlalchemy import SQLAlchemy
from datetime import date
# import json


db = SQLAlchemy()


def setup_db(app):
    """binds a flask application and a SQLAlchemy service"""
    db.app = app
    db.init_app(app)
    # db.create_all()


def setup_db_test(app, database_path=""):
    app.config["SQLALCHEMY_DATABASE_URI"] = database_path
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    db.app = app
    db.init_app(app)
    db.create_all()


# =============================
# performance Association Table
# =============================
Performance = Table(
    "performances",
    db.Model.metadata,
    Column(
        "movie_id", Integer,
        ForeignKey("agency.movies.id")
    ),
    Column(
        "actor_id", Integer,
        ForeignKey("agency.actors.id")
    ),
    Column("actor_fee", Float),
    schema="agency"
)


# =============
# Actors Model
# =============
class Actor(db.Model):
    __tablename__ = "actors"
    __table_args__ = {"schema": "agency"}

    id = Column(Integer, primary_key=True)
    name = Column(String)
    gender = Column(String)
    age = Column(Integer)

    def __init__(self, name, gender, age):
        self.name = name
        self.gender = gender
        self.age = age

    def insert(self):
        db.session.add(self)
        db.session.commit()

    def update(self):
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def format(self):
        return {
            "id": self.id,
            "name": self.name,
            "gender": self.gender,
            "age": self.age
        }


# ===========
# Movie Model
# ===========
class Movie(db.Model):
    __tablename__ = "movies"
    __table_args__ = {"schema": "agency"}

    id = Column(Integer, primary_key=True)
    title = Column(String)
    release_date = Column(Date)
    actors = db.relationship(
        "Actor",
        secondary=Performance,
        backref=db.backref("performances", lazy="joined")
    )

    def __init__(self, title, release_date):
        self.title = title
        self.release_date = release_date

    def insert(self):
        db.session.add(self)
        db.session.commit()

    def update(self):
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def format(self):
        return {
            "id": self.id,
            "title": self.title,
            "release_date": self.release_date
        }


# ==================
# Sample Data Insert
# ==================
def db_init_records():
    """this will initialize the database with some test records."""

    new_actor = Actor(name="Matthew", gender="Male", age=25)
    new_movie = Movie(title="Matthew first Movie", release_date=date.today())
    new_actor.insert()
    new_movie.insert()

    new_performance = Performance.insert().values(
        movie_id=new_movie.id,
        actor_id=new_actor.id,
        actor_fee=500.00
    )
    db.session.execute(new_performance)
    db.session.commit()


# ==================
# Reset DB if needed
# ==================
def drop_db(app):
    """drops the database tables and starts fresh
    can be used to initialize a clean database
    """
    db.app = app
    db.init_app(app)
    db.drop_all()


def reset_db(app):
    """drops the database tables and starts fresh
    can be used to initialize a clean database
    """
    db.app = app
    db.init_app(app)
    db.drop_all()
    db.create_all()
    db_init_records()


def insert_db(app):
    db.app = app
    db.init_app(app)
    db_init_records()
